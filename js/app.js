/**
 * main.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */

angular.module('app', ['blueimp.fileupload', 'ngRoute'])