/**
 * main.js
 *
 * @author Erastus Nathingo <contact@erassy.com>
 * @copyright (c) 2018
 * All rights reserved
 */


angular.module('app', ['ngRoute'])
    .component('fileUpload', {
        templateUrl: 'views/main.html',
        bindings: {
            file: '<', //input binding
            uploadProgress: '&'
        },
        controller: 'videoUploadsCtrl'
    })

    .controller('videoUploadsCtrl', function () {
        this.title = "Hello";
        var $form = $('form');

        /**
         * @description initiates semantic UI funcitons
         */
        (function initSemantic() {
            $('#uploadProgress').progress({
                percent: 0
            });
            $('.ui.embed').embed();
        }());

        var fileUploadMonitor = function () {
            $('#uploadVideo').fileupload({
                autoUpload: false,
                send: function (e, data) {
                    console.log("Send: ", data);
                },
                fail: function (e, data) {
                    console.log("Failed: ", data);
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    console.log("Progress: ", progress)
                    $('#uploadProgress').progress({
                        percent: progress
                    });
                },
                submit: function (e, data) {
                    console.log("Data: ", data);
                }
            }).bind('fileuploadprocessdone', function (e, data) {
                var file = data.files[data.index];
                console.log("File: ",file);
                $('#fileName').text(file.name)
                $('#fileSize').text(file.size + ' Mb')
            })
        }

        fileUploadMonitor();
    })